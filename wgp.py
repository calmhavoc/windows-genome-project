#!/usr/bin/python

import pefile
from sys import argv
import os.path
import optparse
from ctypes import wintypes
import psutil
import win32api
import _winreg

#pe.DIRECTORY_ENTRY_IMPORT[0].imports[1].name
# funtion calls for each dll imported
dlltree=[]
processed = []
header = ""
labels = ""
paths = []
knowndlls=[]
working_appdir = ""
sysdir = ""
plaintext = []

def process_bin(binary):
  global tabs
  global dlltree
  global labels
  global paths
  global basic
  parent,fname = filepath(binary)
  fname = fname
  if parent != '':
    # create new paths containing parent, and append global paths to it
    new_path = paths
    new_path.insert(0,parent)
    pass
  else:
    new_path=paths

  if fname not in processed:
    exists = set_colors(new_path,fname,binary)
    if exists == False:
      labels += '"{0}" [color="yellow"];\n'.format(fname)
    else:
      processed.append(fname)
      imports = get_imports(fname)
      try:
        for subdll in imports:
          dlltree +=  ('"{0}" -> "{1}";\n'.format(fname,subdll),)
          process_bin(subdll)
      except Exception,e:
        pass

def init_paths():
  """ Windows searches 1)Appdir, 2)System dir, 3) 16bit System dir,
  4) Windows dir, 5) Current dir, 6) syspath """

  global knowndlls
  global paths
  knowndlls = set_knowndlls()
  paths = get_syspaths()

def get_syspaths():
  global sysdir
  path = win32api.GetEnvironmentVariable('PATH')
  sysdir = win32api.GetSystemDirectory()
  paths = [win32api.GetSystemDirectory(), win32api.GetWindowsDirectory()] + path.split(';')
  return paths

def set_knowndlls():
    ''' This should be called once at or near startup to set the knowndlls'''
    ### fixme need to also filter and return directorys
    
    knowndlls = []
    hklm = _winreg.ConnectRegistry(None,_winreg.HKEY_LOCAL_MACHINE)
    knowndll_key = _winreg.OpenKey(hklm, r"SYSTEM\CurrentControlSet\Control\Session Manager\KnownDLLs")
    for i in xrange(0,_winreg.QueryInfoKey(knowndll_key)[1]-1):
        knowndlls.append(_winreg.EnumValue(knowndll_key, i)[1].lower())
    return knowndlls

def set_colors(new_path, fname, binary):
    global labels
    global knowndlls
    global basic
  # Add paths to basic where a dll could be placed that will hijack the dll
    if fname in knowndlls:
        labels += '"{0}" [color="green"];\n'.format(fname)
        return True
    if in_memory(binary):
        labels += '"{0}" [color="yellow"];\n'.format(fname)
        return True
#   if file_locked(os.path.join(new_path,fname)):
#     labels += '"{0}" [color="grey"];\n'.format(fname)
    exists,hijackable = file_exists(new_path,fname)
    if hijackable:
        #print fname + " is hijackable: %s, exists: %s" % (hijackable,exists)
        labels += '"{0}" [color="red"];\n'.format(fname)
        return exists
    return True
 
def filepath(binary):
  """ Returns a tuple containing (path,file). If only a filename was given: (,file)"""
  return ("\\".join(binary.split('\\')[:-1]),binary.split('\\')[-1])

def get_imports(filename):
  global paths
  imports = []
  for path in paths:
    try:
        fname = os.path.join(path,filename)
        if os.path.exists(fname):
            try:
                pe = pefile.PE(fname)
                for entry in pe.DIRECTORY_ENTRY_IMPORT:
                    imports.append(entry.dll)
            except Exception,e:
                pass
        return imports
    except Exception,e:
      #print str(e)
      pass

def process_text(path):
  global plaintext
  if path not in plaintext:
    plaintext.append(path)

def file_exists(paths,filename):
  global working_appdir
  global sysdir
  global plaintext
  exists,hijackable=False,False
  for path in paths:
    if os.path.isfile(os.path.join(path,filename)):
        exists = True
        if os.path.isfile(os.path.join(sysdir,filename)):
          if filename not in knowndlls:
            if not os.path.isfile(os.path.join(working_appdir,filename)):
              process_text(os.path.join(working_appdir,filename))
              hijackable = True
  return(exists,hijackable)

def build_digraph():
  digraph = 'digraph G_component_0 {\n'
  digraph += 'graph [ranksep=5, root="test"];\n'
  digraph += 'ration=fill;'
  digraph += 'node [style=filled];'
  digraph += 'sep="10";smoothing="1";'

  
  global dlltree
  global labels
  global header
  digraph += header
  for sets in dlltree:
    digraph += sets
  digraph += labels
  digraph +="}"
  return digraph

def in_memory(binary):
  '''Check if module is already loaded in memory'''
  # This should append to a list so future checks are faster
  # wintypes.windll.kernel32.GetModuleHandleW.restype = wintypes.HMODULE
  wintypes.windll.kernel32.GetModuleHandleW.argtypes = [wintypes.LPCWSTR]
  hMod = wintypes.windll.kernel32.GetModuleHandleW(binary)
  if hMod:return True
  else:return False

def file_locked(binary):
  try:
    f = open(binary,'r')
    f.close()
    return False
  except IOError:
    return True

def get_processes():
  global header
  global working_appdir
  processes = []
  procs = list(psutil.process_iter())

  for proc in procs:
    try:
      processes.append(proc.exe())
    except:pass
  
  for process in processes:
    header += '"root" -> "{0}";\n'.format(process.split('\\')[-1])
    working_appdir = '\\'.join(process.split('\\')[:-1])
    process_bin(process)

if __name__ == "__main__":
  parser = optparse.OptionParser("Usage {0} -b <binary> -w <output to file>\nOR {0} -d To use running processes".format(argv[0]))
  parser.add_option("-b", dest="binary", type="string", help="The binary to process. Do not enter full path as this will search the default path")
  parser.add_option("-w", dest="outfile", type="string", help="If you want to send to a file instead of stdout")
  parser.add_option("-d", "--dump", action="store_true", help="not implemented")
  parser.add_option("-t", "--text", action="store_true", help="Print hijackable dll to stdout")

  (options,args) = parser.parse_args()
  init_paths()


  if options.binary:
    binary = options.binary
    process_bin(binary)
    digraph = build_digraph()

    if options.outfile:
      outfile = options.outfile
      with open(outfile,'w+') as outfile:
        outfile.write(digraph)
    else:
      print digraph


  else:
    get_processes()
    digraph = build_digraph()
    if len(argv) <= 1:
        for item in plaintext:
            print item
      # with open('output.dot', 'w') as f:
      #   f.write(digraph)
    if options.text:
        pass
     

  #    sys.exit(parser.usage)
    if options.outfile:
        outfile = options.outfile
        with open(outfile,'w+') as outfile:
            outfile.write(digraph)
        for item in plaintext:
            print item

    # else:
    #   print digraph
    #   print plaintext

